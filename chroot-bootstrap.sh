#!/bin/bash

# Device hosting the Devuan packages
rootdev=/dev/xvdf

apt-get update

# Installing the necessary packages
# cloud-init will create the default user debian
# and copy the ssh keys given when creating the EC2 instance so we can connect
DEBIAN_FRONTEND=noninteractive apt-get install -y grub-pc linux-image-amd64 openssh-server cloud-init

# Generating the fstab file using the UUID of the partitions
# FYI, currently the device is xvdf but once an EC2 instance will be built based on the AMI,
# usually its device is /dev/xvda
rm /etc/fstab
boot_id=`blkid -s UUID -o value ${rootdev}1`
root_id=`blkid -s UUID -o value ${rootdev}2`
echo "UUID=${root_id} / ext4 defaults 0 0" >> /etc/fstab
echo "UUID=${boot_id} /boot ext4 defaults 1 2" >> /etc/fstab

# Disabling the os prober to avoid it adding the current Debian devices (vxda) in the AMI
echo "GRUB_DISABLE_OS_PROBER=true" >> /etc/default/grub

# Creating the grub config file and installing grub
grub-mkconfig -o /boot/grub/grub.cfg
grub-install --recheck ${rootdev}

# Configuring the network interface or we won't have any network
{
	echo 'auto eth0'
	echo 'iface eth0 inet dhcp'
} >> /etc/network/interfaces

